extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

export(int) var height;
export(int) var width;
export(Vector2) var origin = Vector2(0, 0)

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func get_level_size():
	return Vector2(self.width, self.height)