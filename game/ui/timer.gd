extends Panel

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var t_max = 60.0
var t = 60.0

signal skip_presssed

func set_time_max(t):
	self.t_max = t
	self.set_time_left(t)

func set_time_left(t):
	if t == null && self.t > self.t_max:
		self.t = self.t_max
	if t <= self.t_max:
		self.t = t
	get_node('timeleft').text = self.format_time(self.t)

func format_time(t):
	var minutes = t / 60;
	var seconds = int(t) % 60;
	return '%d:%02d' % [minutes, seconds]
	
func _draw():
	pass

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_skip_pressed():
	emit_signal('skip_pressed')
