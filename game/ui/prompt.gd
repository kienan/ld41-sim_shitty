extends LineEdit


# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


# This only fires when a user presses enter
func _on_prompt_text_entered(new_text):
	print(text)
	pass # replace with function body


# Here we can do tab complete, ctrl-c clear line, etc.
# ctrl-r search
func _on_prompt_gui_input(ev):
	# Check if we have focus on the prompt
	if not self.has_focus():
		return
	if ev is InputEventKey and ev.pressed:
		if ev.scancode == KEY_C:
			# This is Ctrl-c
			if ev.get_scancode_with_modifiers() == 268435523:
				# Emit signal on clear?
				self.clear()
				# How to mark input as consumed
		if ev.scancode == KEY_TAB:
			# @TODO: Autocomplete current text if possible
			print("@TODO: Implemement auto-complete")
		if ev.scancode == KEY_R:
			# This is Ctrl-r
			if ev.get_scancode_with_modifiers() == 268435538:
				# Emit signal to start search
				print("@TODO Implementare Ctrl-r Search")