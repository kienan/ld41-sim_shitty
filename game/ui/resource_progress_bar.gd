extends VBoxContainer

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

export(Texture) var icon;
export(String)  var name;
export(Texture) var progress_texture;

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	if this.icon:
		get_node('TextureRect').texture = this.icon
	

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
