extends Panel

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var resources = [
	'population',
	'jobs',
	'food',
	'resources',
	'effort',
	'goods'
]

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func update_resources(r):
	print(self.resources)
	for i in self.resources:
		if not i:
			continue
		var v = r[i]
		get_node('HBoxContainer/%s/progress' %i).value = v
		get_node('HBoxContainer/%s' %i).hint_tooltip = '%s: %d' %[i, v]

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
