GODOT?=godot3-server
EXPORT_TEMPLATE_SOURCE?=https://downloads.tuxfamily.org/godotengine/3.0.2/Godot_v3.0.2-stable_export_templates.tpz
BUILD_DIR?=build
GODOT_PROJECT_PATH?=game
PROJECT_NAME?=game
GODOT_TEMPLATE_DIR?=~/.local/share/godot/templates
GODOT_TEMPLATE_VERSION?=3.0.2.stable
TARGET_HTML5?=HTML5
TARGET_WINDOWS?=Windows Desktop
TARGET_MACOSX?=Mac OSX
TARGET_LINUX?=Linux/X11
WGET_ARGS?=-q

GODOT_GUT_SCRIPT?=addons/gut/gut_cmdln.gd
GODOT_GUT_GDIR=res://tests/unit,res://tests/integration

build-dir:
	mkdir -p "$(BUILD_DIR)"

clean:
	rm -rf "$(BUILD_DIR)"/

export-all: export-html5 export-windows export-osx export-linux

export-pack: build-dir
	$(GODOT) --path "$(GODOT_PROJECT_PATH)" --export_debug --export "pack" "../$(BUILD_DIR)/$(PROJECT_NAME).pck"

export-html5: build-dir
	mkdir -p "$(BUILD_DIR)/html"
	$(GODOT) --path "$(GODOT_PROJECT_PATH)" --export_debug --export "$(TARGET_HTML5)" "../$(BUILD_DIR)/html/$(PROJECT_NAME).html"
	zip -j -r "$(BUILD_DIR)/$(PROJECT_NAME)-html5.zip" "$(BUILD_DIR)/html"
	rm -rf "$(BUILD_DIR)/html"

export-windows: build-dir
	$(GODOT) --path "$(GODOT_PROJECT_PATH)" --export_debug --export "$(TARGET_WINDOWS)" $(PROJECT_NAME).exe
	mkdir -p "$(BUILD_DIR)/win/"
	mv -t "$(BUILD_DIR)/win/" $(GODOT_PROJECT_PATH)/$(PROJECT_NAME).exe* $(GODOT_PROJECT_PATH)/$(PROJECT_NAME).pck

export-osx: build-dir
	$(GODOT) --path "$(GODOT_PROJECT_PATH)" --export_debug --export "$(TARGET_MACOSX)" $(PROJECT_NAME).osx
	mkdir -p "$(BUILD_DIR)/osx/"
	# Data is bundled into $(PROJECT_NAME).osx
	mv -t "$(BUILD_DIR)/osx/" $(GODOT_PROJECT_PATH)/$(PROJECT_NAME).osx*

export-linux: build-dir
	$(GODOT) --path $(GODOT_PROJECT_PATH) --export_debug --export "$(TARGET_LINUX)" $(PROJECT_NAME)-x11
	mkdir -p "$(BUILD_DIR)/x11/"
	mv -t "$(BUILD_DIR)/x11/" $(GODOT_PROJECT_PATH)/$(PROJECT_NAME)-x11*

install-templates:
	mkdir -p $(GODOT_TEMPLATE_DIR)
	wget $(WGET_ARGS) $(EXPORT_TEMPLATE_SOURCE) -O templates.tpz
	unzip templates.tpz
	mv templates/ $(GODOT_TEMPLATE_DIR)/$(GODOT_TEMPLATE_VERSION)
	rm templates.tpz

tests:
	$(GODOT) -d -s $(GODOT_GUT_SCRIPT) --path $(GODOT_PROJECT_PATH) -gdir=$(GODOT_GUT_GDIR) -gexit

